# README #

A Ruzzle solver written with C++/Qt, using [tries](http://en.wikipedia.org/wiki/Trie) and a floodfill-like algorithm to find quickly the words in the schema.