#ifndef ALPHATRIE_HPP_INCLUDED
#define ALPHATRIE_HPP_INCLUDED

#include <memory>
#include <stdexcept>
#include <cctype>
#include <cstring>
#include <cassert>

// Trie node for alphabetic characters (a-z)
class AlphaTrie
{
    // Pointers for the next trie nodes
    std::auto_ptr<AlphaTrie> next[26];
    // true if the current node marks the end of a word
    bool final;

    // Gets the letter-index (range [0, 25]) of the first valid letter
    // in the word; moves the pointer to skip all invalid characters
    // Returns -1 if the word ended before finding any good character.
    int GetFirstIdxMovePtr(const char * & word) const
    {
        char cur=*word;
        // Skip the garbage
        while(cur && !std::isalpha(cur))
        {
            cur=0;
            // Fix italian lowercase accented letters (works for UTF-8 input)
            if((unsigned char)*word==0xc3)
            {
                switch((unsigned char)word[1])
                {
                case 0xa0:
                case 0xa1:
                    cur='a';
                    break;
                case 0xa8:
                case 0xa9:
                    cur='e';
                    break;
                case 0xac:
                case 0xad:
                    cur='i';
                    break;
                case 0xb2:
                case 0xb3:
                    cur='o';
                    break;
                case 0xb9:
                case 0xc0:
                    cur='u';
                    break;
                }
            }
            word++;
            if(cur==0)
                cur=*word;
        }
        // If we reached the end of the word, return a negative value
        if(!cur)
            return -1;
        // Calculate the index
        int idx=std::tolower(cur)-'a';
        assert(idx>=0 && idx<26);
        return idx;
    }

    // Disable copy construction
    AlphaTrie(const AlphaTrie &);

public:

    // Construct a new node; by default it's a non-terminal one.
    AlphaTrie()
        : final(false)
    {}

    // True if the node matches the end of a word
    bool IsFinal() const
    {
        return final;
    }

    // Gets the next trie node from the given char
    // (mainly as interface from the outside)
    const AlphaTrie * GetNextTrie(char ch) const
    {
        int idx=std::tolower(ch)-'a';
        if(idx<0 || idx>=26)
            throw std::invalid_argument("Invalid character");
        return next[idx].get();
    }

    // Adds a word to the trie
    void AddWord(const char * word)
    {
        // Get the first character-index of word
        int idx=GetFirstIdxMovePtr(word);
        if(idx<0)
        {
            // If the word ended, then the current node is final
            final=true;
            return;
        }
        // If there's no child node for it yet, create it
        if(next[idx].get()==NULL)
            next[idx].reset(new AlphaTrie());
        // Recurse for the next letter
        next[idx]->AddWord(word+1);
    }

    // Check if the given word is present
    bool IsWordPresent(const char * word) const
    {
        // Get the terminal trie treating word as a prefix
        const AlphaTrie * pt=GetPrefixTrie(word);
        // Check if it actually exists and is final
        return pt!=NULL && pt->final;
    }

    // Find the terminal trie node for the given prefix (if present)
    const AlphaTrie * GetPrefixTrie(const char * prefix) const
    {
        int idx=GetFirstIdxMovePtr(prefix);
        // If the word has ended, this *is* the terminal trie
        if(idx<0)
            return this;
        // No node for the current letter => the prefix is unknown
        if(next[idx].get()==NULL)
            return NULL;
        // Recurse on the next letter
        return next[idx]->GetPrefixTrie(prefix+1);
    }

    // Count the nodes present under this trie node
    unsigned int CountNodes()
    {
        // This is one node...
        unsigned int ret=1;
        // ... then recurse!
        for(unsigned int i=0; i<26; i++)
            if(next[i].get()!=NULL)
                ret+=next[i]->CountNodes();
        return ret;
    }

    // Resets the node
    void reset()
    {
        for(unsigned int i=0; i<26; i++)
            next[i].reset();
        final=false;
    }
};

#endif
