#include <QApplication>
#include <QTime>
#include "solverdialog.hpp"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qsrand(QTime::currentTime().msec());
    SolverDialog w;
    w.show();

    return a.exec();
}
