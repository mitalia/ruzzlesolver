#include "ruzzlescheme.hpp"
#include "alphatrie.hpp"

#include <vector>
#include <string>
#include <stdexcept>
#include <stack>
#include <iostream>

void RuzzleScheme::Result::PrettyPrint(std::ostream & os) const
{
    os<<word<<"\n\n";
    // Prepare a 2D "framebuffer" for the output filled with dots
    std::string outBuf((boardWidth+1)*boardHeight,'.');
    // Replace the dots at the right positions with newlines
    for(unsigned int i=0; i<boardHeight; i++)
        outBuf[i*(boardWidth+1)+boardWidth]='\n';
    // Replace the letters of the word in the buffer at the correct positions
    for(unsigned int i=0; i<word.size(); i++)
        outBuf[positions[i].y*(boardWidth+1)+positions[i].x]=word[i];
    // Write out the buffer
    os<<outBuf;
}

void RuzzleScheme::InsertRow(std::string rowData, unsigned int row)
{
    if(row>=height)
        throw std::out_of_range("row must be less than the scheme height");
    if(rowData.size()>width)
        throw std::out_of_range("rowData must be shorter than the scheme width");
    std::copy(rowData.begin(), rowData.begin()+4, &At(0,row));
}

void RuzzleScheme::FindElements(const AlphaTrie & trie, std::vector<Result> & out)
{
    if(!CheckCharacters())
        throw std::runtime_error("Invalid characters in the scheme.");
    posStack.clear();
    // Start a "floodfill-like" search starting from each cell
    for(unsigned int y=0; y<height; y++)
        for(unsigned int x=0; x<width; x++)
            FindElementsHelper(x, y, 0, trie, out);
}

void RuzzleScheme::FindElementsHelper(unsigned int x, unsigned int y, unsigned int wordLen, const AlphaTrie & trie, std::vector<Result> & out)
{
    char & cur=At(x,y);
    // We switch on the high bit of a character to mark that it has already
    // been considered in the current search; so, if we find it already marked,
    // just exit
    if((unsigned char)cur &0x80)
        return;

    // Put the current letter at the end of the word buffer
    wordBuf[wordLen]=cur;
    // Get the relevant trie from the current letter
    const AlphaTrie * next=trie.GetNextTrie(cur);
    // If there's no word that goes like this, this is a dead end
    if(next==NULL)
        return;
    // Push on the stack the current position
    posStack.push_back(Pos(x,y));
    // Mark the current character
    cur=(unsigned char)cur|0x80;
    if(next->IsFinal())
    {
        // We got a full word! Add it to the results
        out.push_back(
            Result(
                std::string(wordBuf.begin(), wordBuf.begin()+wordLen+1),
                posStack,
                width,
                height));
    }
    // Proceed with recursive, 8-neighboors search
    if(y>0)
        FindElementsHelper(x, y-1, wordLen+1, *next, out);
    if(y<height-1)
        FindElementsHelper(x, y+1, wordLen+1, *next, out);
    if(x>0)
    {
        FindElementsHelper(x-1, y, wordLen+1, *next, out);
        if(y>0)
            FindElementsHelper(x-1, y-1, wordLen+1, *next, out);
        if(y<height-1)
            FindElementsHelper(x-1, y+1, wordLen+1, *next, out);
    }
    if(x<width-1)
    {
        FindElementsHelper(x+1, y, wordLen+1, *next, out);
        if(y>0)
            FindElementsHelper(x+1, y-1, wordLen+1, *next, out);
        if(y<width-1)
            FindElementsHelper(x+1, y+1, wordLen+1, *next, out);
    }
    // Unmark the character
    cur=(unsigned char)cur & ~0x80;
    // Pop the position
    posStack.pop_back();
}
