#ifndef RUZZLESCHEME_HPP_INCLUDED
#define RUZZLESCHEME_HPP_INCLUDED

#include <vector>
#include <string>
#include <stdexcept>
#include <iosfwd>
#include <algorithm>

#include "alphatrie.hpp"

// Represents a Ruzzle game scheme
class RuzzleScheme
{
public:
    // Classic x-y position (only positive integers)
    struct Pos
    {
        unsigned int x;
        unsigned int y;

        Pos(unsigned int x, unsigned int y) : x(x), y(y) {}
    };

    // A result is composed of the word and its positions on the board
    // boardWidth and boardHeight required for prettyprinting
    struct Result
    {
        std::string word;
        std::vector<Pos> positions;
        unsigned int boardWidth, boardHeight;
        unsigned int score = 0;

        Result(const std::string & word, const std::vector<Pos> & positions, unsigned int boardWidth, unsigned int boardHeight)
            : word(word), positions(positions), boardWidth(boardWidth), boardHeight(boardHeight)
        {
            if(word.size()!=positions.size())
                throw std::invalid_argument("word and positions must have the same size");
        }

        void PrettyPrint(std::ostream & os) const;

        bool operator<(const Result &right) const
        {
            return score < right.score;
        }
    };

private:
    // Size of the board
    unsigned int width, height;
    // Backing storage for the board
    std::vector<char> storage;
    // Buffer for the words we are finding in the recursive search
    std::vector<char> wordBuf;
    // Positions stack (were the positions currently examined are pushed/popped)
    std::vector<Pos> posStack;

    // Recursive helper for FindElements; calls itself to perform a recursive,
    // 8-neighboors search for words on the board
    void FindElementsHelper(unsigned int x, unsigned int y, unsigned int wordLen, const AlphaTrie & trie, std::vector<Result> & out);

public:

    unsigned int GetWidth() const
    {
        return width;
    }

    unsigned int GetHeight() const
    {
        return height;
    }

    // Returns the character at the given position
    char& At(unsigned int x, unsigned int y)
    {
        return storage[x+y*width];
    }

    const char& At(unsigned int x, unsigned int y) const
    {
        return storage[x+y*width];
    }

    // Inserts the specified scheme row
    void InsertRow(std::string rowData, unsigned int row);

    // Check for unwanted characters
    bool CheckCharacters()
    {
        for(unsigned int i=0; i<width*height; i++)
            if(!std::isalpha(storage[i]))
                return false;
        return true;
    }

    RuzzleScheme(unsigned int width=4, unsigned int height=4)
        : width(width),
          height(height),
          storage(width*height, 0),
          wordBuf(width*height, 0)
    {
        if(width<2 || height<2)
            throw std::invalid_argument("The scheme must be 2x2 at minimum.");
    }

    // Finds the words in the scheme, put the results into the out vector.
    void FindElements(const AlphaTrie & trie, std::vector<Result> & out);
};

// output operator for a result - delegate to PrettyPrint
inline std::ostream & operator<<(std::ostream& os, const RuzzleScheme::Result & obj)
{
    obj.PrettyPrint(os);
    return os;
}

// output operator for the scheme; prints the scheme
inline std::ostream & operator<<(std::ostream& os, const RuzzleScheme & obj)
{
    for(unsigned int y=0; y<obj.GetHeight(); y++)
    {
        for(unsigned int x=0; x<obj.GetWidth(); x++)
            os<<obj.At(x,y);
        os<<'\n';
    }
    return os;
}
#endif
