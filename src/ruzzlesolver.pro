#-------------------------------------------------
#
# Project created by QtCreator 2013-01-31T22:17:01
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ruzzlesolver
TEMPLATE = app

win32-g++ {
    QMAKE_LFLAGS += -static
}

SOURCES += main.cpp\
    ruzzlescheme.cpp \
    solverdialog.cpp

HEADERS  += \
    ruzzlescheme.hpp \
    alphatrie.hpp \
    solverdialog.hpp

FORMS    += \
    solverdialog.ui
