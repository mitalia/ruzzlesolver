#include "solverdialog.hpp"
#include "ui_solverdialog.h"
#include "ruzzlescheme.hpp"
#include "alphatrie.hpp"

#include <QGridLayout>
#include <QDialog>
#include <QVector>
#include <QLineEdit>
#include <QString>
#include <QMessageBox>
#include <QFile>
#include <QTextStream>
#include <QtCore/qmath.h>

#include <string>

SolverDialog::SolverDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SolverDialog)
{
    ui->setupUi(this);
    delete ui->lblPlaceholder;
    ui->lblPlaceholder=NULL;
    ui->itwResults->setModel(&resModel);
    QObject::connect(ui->itwResults->selectionModel(),SIGNAL(currentChanged ( const QModelIndex &, const QModelIndex &)), this, SLOT(itwResults_selectionModel_currentChanged ( const QModelIndex &, const QModelIndex &)));
    recreateGrid(this->ui->spnSide->value());
    loadDictionary("dictionary") || loadDictionary("/usr/share/dict/italian");
}

void SolverDialog::itwResults_selectionModel_currentChanged ( const QModelIndex & current, const QModelIndex & previous ) 
{
    (void)previous;
    cleanLEBGs();
    if(current.row()<0 || current.row() > (int)resModel.results.size())
        return;
    int side=ui->spnSide->value();
    std::vector<RuzzleScheme::Pos> & p=resModel.results[current.row()].positions;
    QColor start("orangered"), end("yellow");
    for(int i=0; i<(int)p.size(); i++)
    {
        QColor col;
        if(p.size()==1)
            col=start;
        else
            col=QColor(
                start.red()+(end.red()-start.red())*i/(p.size()-1),
                start.green()+(end.green()-start.green())*i/(p.size()-1),
                start.blue()+(end.blue()-start.blue())*i/(p.size()-1));
        texts[p[i].x+p[i].y*side]->setStyleSheet(QString("background-color: ")+col.name());
    }
}

void SolverDialog::cleanLEBGs()
{
    for(int i=0; i<texts.size(); i++)
        texts[i]->setStyleSheet("");
}

void SolverDialog::recreateGrid(unsigned int side)
{
    for(int i=0; i<texts.size(); i++)
        delete texts[i];
    texts.clear();
    resModel.beginReset();
    resModel.results.clear();
    resModel.endReset();
    QGridLayout * grpLayout=dynamic_cast<QGridLayout *>(ui->grpSchema->layout());
    for(unsigned int i=0; i<side*side; i++)
    {
        QLineEdit * le=new SingleCharLE(this);
        le->clear();
        le->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
        le->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        QObject::connect(le, SIGNAL(textEdited(const QString&)), this, SLOT(le_textEdited(const QString &)));
        grpLayout->addWidget(le,i/side,i%side);
        texts.push_back(le);
    }
}

bool SolverDialog::loadDictionary(const QString & path)
{
    QFile dictFile(path);
    double freq[26]={0.};
    double tot=0.;
    if(!dictFile.open(QFile::ReadOnly)) return false;

    QTextStream is(&dictFile);
    QString row, last;
    trie.reset();
    while(!(row=is.readLine()).isNull())
    {
        const char * seq=row.toLower().normalized(QString::NormalizationForm_KD).toLatin1();
        trie.AddWord(seq);
        for(; *seq; seq++)
        {
            if('a'<=*seq && *seq<='z')
                freq[*seq-'a']++;
            tot++;
        }
    }

    weightedCharacters.clear();
    for(int i=0; i<26; i++)
        weightedCharacters+=QString(qCeil(freq[i]*256./tot), 'A'+i);
    return true;
}

bool SolverDialog::solveScheme()
{
    cleanLEBGs();
    int side=ui->spnSide->value();
    bool ret=false;
    RuzzleScheme rs(side, side);
    char * schemePtr=&rs.At(0,0);
    for(int i=0; i<texts.size(); i++)
    {
        QString str=texts[i]->text();
        if(str.length()==0)
            str="\0";
        schemePtr[i]=str[0].toLatin1();
    }
    resModel.beginReset();
    resModel.results.clear();
    if(!rs.CheckCharacters())
        ret=false;
    else
    {
        ret=true;
        rs.FindElements(trie, resModel.results);
    }
    resModel.endReset();
    return ret;

}

void SolverDialog::on_btnRandom_clicked()
{
    for(int i=0; i<texts.size(); i++)
        texts[i]->setText(QString(weightedCharacters[qrand()%weightedCharacters.size()]));
    solveScheme();
}

void SolverDialog::le_textEdited(const QString & text)
{
    if(text.length()!=0)
    {
        QLineEdit * le=dynamic_cast<QLineEdit *>(QObject::sender());
        QString str=text;
        QString dec=text[text.size()-1].decomposition();
        if(!dec.isEmpty())
            str=dec;
        str.resize(1);
        if(str[0].isLetter())
        {
            le->setText(QString(str[0].toUpper()));
            texts[(texts.indexOf(le)+1)%texts.size()]->setFocus();
        }
        else
            le->clear();
    }
    solveScheme();
}

void SolverDialog::on_spnSide_valueChanged(int value)
{
    recreateGrid(value);
}

SolverDialog::~SolverDialog()
{
    delete ui;
}
