#ifndef SOLVERDIALOG_HPP
#define SOLVERDIALOG_HPP

#include <QDialog>
#include <QVector>
#include <QLineEdit>
#include <QString>
#include <QAbstractListModel>
#include <QModelIndex>
#include <QVariant>
#include <QKeyEvent>
#include <QString>

#include "ruzzlescheme.hpp"
#include "alphatrie.hpp"

class SchemeResultsModel : public QAbstractListModel
{
    Q_OBJECT

public:
    std::vector<RuzzleScheme::Result> results;

    int rowCount ( const QModelIndex & parent = QModelIndex() ) const
    {
        if (parent.isValid())
            return 0;
        return results.size();
    }

    QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const
    {
        if(role!=Qt::DisplayRole)
            return QVariant();
        if(index.row()<0 || index.row()>=(int)results.size())
            return QVariant();
        return QString(results[index.row()].word.c_str()).toLower();
    }

    void beginReset()
    {
        beginResetModel();
    }

    void endReset()
    {
        endResetModel();
    }

};

class SingleCharLE : public QLineEdit
{
    Q_OBJECT

public:
    explicit SingleCharLE(QWidget *parent = 0)
        : QLineEdit(parent)
    {};

protected:
    void keyPressEvent(QKeyEvent *event)
    {
        if(!event->text().isEmpty())
            this->clear();
        QLineEdit::keyPressEvent(event);
    }
};

namespace Ui {
class SolverDialog;
}

class SolverDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit SolverDialog(QWidget *parent = 0);
    ~SolverDialog();
    
protected:
    Ui::SolverDialog *ui;
    QVector<QLineEdit *> texts;

    AlphaTrie trie;
    SchemeResultsModel resModel;

    QString weightedCharacters;

    void recreateGrid(unsigned int side);
    bool loadDictionary(const QString & path);

    void cleanLEBGs();
    bool solveScheme();

public slots:
    // Automatic slots - connected by iuc
    virtual void on_spnSide_valueChanged(int value);
    virtual void on_btnRandom_clicked();

    // "Manual" slots
    virtual void itwResults_selectionModel_currentChanged ( const QModelIndex & current, const QModelIndex & previous );
    virtual void le_textEdited(const QString &);
};

#endif // SOLVERDIALOG_HPP
